﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireShotController : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        Destroy(gameObject);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.name == "Switch")
        {
            SwitchController switchController = collision.gameObject.GetComponent<SwitchController>();
            switchController.Active();
            Destroy(gameObject);
        }
        if (collision.name == "Door" || collision.name == "Walls"  || collision.name == "FakeSwitch")
        {
            Destroy(gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
public class NinjaController : MonoBehaviour, IAnimationCompleted
{

    public GameObject FirePrefab;
	public float speed = 5.0f;
	public float movementThreshold = 0.5f;
    public float projSpeed = 5f;

    private Vector2 inputDirection;
	private Rigidbody2D _rigidbody2D;
	private Animator _animator;
    private bool attack = false;

	private void Start()
	{
		_rigidbody2D = GetComponent<Rigidbody2D>();
		_animator = GetComponent<Animator>();
	}

    public void AnimationCompleted(int shortHashName)
    {
        attack = false;
    }

    private void Attack(Vector2 direction)
    {
        attack = true;
        _animator.SetTrigger("Attack");
        _rigidbody2D.velocity = new Vector2();
        if (FirePrefab != null)
        {
            //TODO: Instatiate the fire bullet in facing position and add the velocity... none of this code is working by now
            GameObject fireInstance = Instantiate(FirePrefab, transform.position, transform.rotation);

            fireInstance.GetComponent<Rigidbody2D>().AddForce(direction * projSpeed);
            fireInstance.GetComponent<Rigidbody2D>().velocity = new Vector2(direction.x * projSpeed, direction.y * projSpeed);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (DragController.Instance.IsDragged())
        {
            attack = true;
            Vector2 dragDirection = new Vector2();
            switch (DragController.Instance.dragDirection)
            {

                case DragController.DragDirection.UP:
                    dragDirection = Vector2.up;
                    Attack(dragDirection);
                    break;
                case DragController.DragDirection.RIGHT:
                    dragDirection = Vector2.right;
                    Attack(dragDirection);
                    break;
                case DragController.DragDirection.DOWN:
                    dragDirection = -Vector2.up;
                    Attack(dragDirection);
                    break;
                case DragController.DragDirection.LEFT:
                    dragDirection = -Vector2.right;
                    Attack(dragDirection);
                    break;
            }
        }
        if(!attack)
        {
            Vector2 MoveDirection = new Vector2();
            switch(InputController.Instance.direction)
            {
                case InputController.DIRECTIONS.UP:
                    MoveDirection.y = 1;
                    break;
                case InputController.DIRECTIONS.RIGHT:
                    MoveDirection.x = 1;
                    break;  
                case InputController.DIRECTIONS.DOWN:
                    MoveDirection.y = -1;
                    break;
                case InputController.DIRECTIONS.LEFT:
                    MoveDirection.x = -1;
                    break;
                case InputController.DIRECTIONS.NONE:
                    MoveDirection.x = 0;
                    MoveDirection.y = 0;
                    break;
              
               
                
            }
            if (MoveDirection.magnitude > movementThreshold)
            {
                _rigidbody2D.velocity = new Vector2(MoveDirection.x * speed, MoveDirection.y * speed);

                // Set the input values on the animator
                _animator.SetFloat("inputX", MoveDirection.x);
                _animator.SetFloat("inputY", MoveDirection.y);
                _animator.SetBool("isWalking", true);
            }
            else
            {
                _rigidbody2D.velocity = new Vector2();
                _animator.SetBool("isWalking", false);
            }
        }

    }
}

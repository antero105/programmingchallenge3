﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InputClassifier", menuName = "Input Classifier")]
public class InputClassifier : ScriptableObject
{

    public enum InputType
    {
        UP,
        RIGHT,
        DOWN,
        LEFT
    }
    public InputType input;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputController : Singleton<InputController>
{

    [HideInInspector]
    public enum DIRECTIONS
    {
        NONE,
        UP,
        RIGHT,
        DOWN,
        LEFT
    }
    public DIRECTIONS direction;

    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    private void Start()
    {
        //Fetch the Raycaster from the GameObject (the Canvas)
        m_Raycaster = GetComponent<GraphicRaycaster>();
        //Fetch the Event System from the Scene
        m_EventSystem = FindObjectOfType<EventSystem>();
    }

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                //Set up the new Pointer Event
                m_PointerEventData = new PointerEventData(m_EventSystem);
                //Set the Pointer Event Position to that of the mouse position
                m_PointerEventData.position = Input.mousePosition;

                //Create a list of Raycast Results
                List<RaycastResult> results = new List<RaycastResult>();

                //Raycast using the Graphics Raycaster and mouse click position
                m_Raycaster.Raycast(m_PointerEventData, results);

                //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
                foreach (RaycastResult result in results)
                {
                    InputClassifier inputClassifier = result.gameObject.GetComponent<InputType>().inputClassifier;
                    if (inputClassifier != null)
                    {
                        switch (inputClassifier.input)
                        {
                            case InputClassifier.InputType.UP:
                                direction = DIRECTIONS.UP;
                                break;
                            case InputClassifier.InputType.RIGHT:
                                direction = DIRECTIONS.RIGHT;
                                break;
                            case InputClassifier.InputType.DOWN:
                                direction = DIRECTIONS.DOWN;
                                break;
                            case InputClassifier.InputType.LEFT:
                                direction = DIRECTIONS.LEFT;
                                break;
                        }
                    }
                }
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                direction = DIRECTIONS.NONE;
            }

        }



    }
   



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragController : Singleton<DragController>
{

    public float minDistance = 20.0f;
    public enum DragDirection
    {
        NONE,
        UP,
        RIGHT,
        DOWN,
        LEFT
    }

    public DragDirection dragDirection;
    public void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
                if (touch.deltaPosition.x > minDistance)
                {
                    // RIGHT
                    dragDirection = DragDirection.RIGHT;
                }

                if (touch.deltaPosition.x < -minDistance)
                {
                    // LEFT
                    dragDirection = DragDirection.LEFT;
                }

                if (touch.deltaPosition.y < -minDistance)
                {
                    // DOWN
                    dragDirection = DragDirection.DOWN;
                }

                if (touch.deltaPosition.y > minDistance)
                {
                    // UP
                    dragDirection = DragDirection.UP;
                }
            }
            else
            {
                dragDirection = DragDirection.NONE;
            }
        }

    }

    public bool IsDragged()
    {
        return dragDirection != DragDirection.NONE;
    }


}
